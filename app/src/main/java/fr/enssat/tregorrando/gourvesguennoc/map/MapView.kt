package fr.enssat.tregorrando.gourvesguennoc.map

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import org.osmdroid.views.MapView

@Composable
fun MapView(
    modifier: Modifier = Modifier,
    onLoad: ((map: MapView) -> Unit)? = null,
    hike: String
) {
    val mapViewState = MapLifecycle.rememberMapViewWithLifecycle(hike)

    AndroidView(
        factory = { mapViewState }, modifier = modifier
    ) { mapView -> onLoad?.invoke(mapView) }
}
