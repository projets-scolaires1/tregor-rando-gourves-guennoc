package fr.enssat.tregorrando.gourvesguennoc.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HikeCollection(
    val type: String,
    val features: List<Hike>
)
