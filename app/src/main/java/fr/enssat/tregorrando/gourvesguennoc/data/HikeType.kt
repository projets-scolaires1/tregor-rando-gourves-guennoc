package fr.enssat.tregorrando.gourvesguennoc.data

import com.squareup.moshi.Json

enum class HikeType(name: String) {
    ALL_HIKE_TYPE("Tous les parcours"),

    @Json(name = "Pédestre")
    PEDESTRIAN("Pédestre"),

    @Json(name = "VTT")
    MOUNTAIN_BIKE("VTT"),

    @Json(name = "Vélo")
    BIKE("Vélo"),

    @Json(name = "Equestre")
    HORSE("Equestre")
}
