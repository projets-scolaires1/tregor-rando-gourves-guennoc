package fr.enssat.tregorrando.gourvesguennoc.data

data class HikeGeometry(
    val type: String,
    val coordinates: List<List<List<Double>>>
) {
    override fun toString(): String {
        return """
        {
            "type": "$type",
            "coordinates": $coordinates
        }
        """.trimIndent()
    }
}
