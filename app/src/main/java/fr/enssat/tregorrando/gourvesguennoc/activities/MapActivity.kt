package fr.enssat.tregorrando.gourvesguennoc.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import fr.enssat.tregorrando.gourvesguennoc.map.MapView
import fr.enssat.tregorrando.gourvesguennoc.network.DatarmorApiStatus
import fr.enssat.tregorrando.gourvesguennoc.ui.theme.TregorRandoTheme
import fr.enssat.tregorrando.gourvesguennoc.viewmodels.MapViewModel

class MapActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mapViewModel = MapViewModel(intent)

        setContent {
            TregorRandoTheme {
                Surface(
                    color = MaterialTheme.colors.background
                ) {
                    val status by mapViewModel.status.observeAsState(DatarmorApiStatus.ERROR)
                    val hike by mapViewModel.properties.observeAsState()

                    when (status) {
                        DatarmorApiStatus.LOADING -> {
                            Column(
                                modifier = Modifier.fillMaxSize(),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                CircularProgressIndicator(color = Color.White)
                            }
                        }
                        DatarmorApiStatus.DONE -> {
                            hike?.let { MapView(hike = it.toString()) }
                        }
                        DatarmorApiStatus.ERROR -> {
                            Column(
                                modifier = Modifier.fillMaxSize(),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Text(
                                    text = "Une erreur est survenue. Etes-vous connecté à Internet ?",
                                    style = MaterialTheme.typography.body2,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp, vertical = 24.dp)
                                )
                            }
                        }
                    }

                    // Back to the hike list activity
                    Button(
                        onClick = {
                            val intent = Intent(this@MapActivity, MenuActivity::class.java)
                            startActivity(intent)
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(30.dp)
                    ) {
                        Text("Sélectionner une randonnée")
                    }
                }
            }
        }
    }
}
