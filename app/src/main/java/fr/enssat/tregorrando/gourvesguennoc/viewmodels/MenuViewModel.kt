package fr.enssat.tregorrando.gourvesguennoc.viewmodels

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.enssat.tregorrando.gourvesguennoc.data.Hike
import fr.enssat.tregorrando.gourvesguennoc.data.HikeType
import fr.enssat.tregorrando.gourvesguennoc.network.DatarmorApiStatus
import fr.enssat.tregorrando.gourvesguennoc.network.DatarmorNetworkService
import kotlinx.coroutines.launch

class MenuViewModel : ViewModel() {
    private val _status = MutableLiveData<DatarmorApiStatus>()
    val status: LiveData<DatarmorApiStatus>
        get() = _status

    private val _properties = MutableLiveData<List<Hike>>()
    val properties: LiveData<List<Hike>>
        get() = _properties

    private val _filterByType = MutableLiveData<HikeType>()
    var filterByType: MutableLiveData<HikeType> = _filterByType

    private val _filterByName = MutableLiveData("")
    var filterByName: MutableLiveData<String> = _filterByName

    init {
        viewModelScope.launch {
            _status.value = DatarmorApiStatus.LOADING
            Log.d("DATARMOR_API", "launch")

            try {
                _properties.value = DatarmorNetworkService.getAllHikes()
                _status.value = DatarmorApiStatus.DONE
                _filterByType.value = HikeType.ALL_HIKE_TYPE
                _filterByName.value = ""
            } catch (e: Exception) {
                _properties.value = null
                _status.value = DatarmorApiStatus.ERROR
                _filterByType.value = HikeType.ALL_HIKE_TYPE
                _filterByName.value = ""
            }
        }
    }
}
