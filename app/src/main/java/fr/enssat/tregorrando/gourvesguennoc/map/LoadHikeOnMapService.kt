package fr.enssat.tregorrando.gourvesguennoc.map

import org.osmdroid.bonuspack.kml.KmlDocument
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.FolderOverlay

class LoadHikeOnMapService {
    fun loadHikePathById(
        mapView: MapView,
        hike: String
    ) {
        val kmlDocument = KmlDocument()
        kmlDocument.parseGeoJSON(hike)

        val kmlOverlay =
            kmlDocument.mKmlRoot.buildOverlay(mapView, null, null, kmlDocument) as FolderOverlay
        mapView.overlays.add(kmlOverlay)

        // Set map center from the KML document and zoom level
        mapView.controller.setCenter(kmlDocument.mKmlRoot.boundingBox.centerWithDateLine)
        mapView.controller.setZoom(15.0)
    }
}
