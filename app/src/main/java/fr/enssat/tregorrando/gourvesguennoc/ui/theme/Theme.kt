package fr.enssat.tregorrando.gourvesguennoc.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = Teal,
    primaryVariant = Amber,
    secondary = LightTeal,
    background = DarkBG
)

private val LightColorPalette = lightColors(
    primary = DarkTeal,
    primaryVariant = Amber,
    secondary = LightTeal
)

@Composable
fun TregorRandoTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
