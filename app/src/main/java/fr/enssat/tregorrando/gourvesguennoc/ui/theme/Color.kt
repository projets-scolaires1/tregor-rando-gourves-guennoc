package fr.enssat.tregorrando.gourvesguennoc.ui.theme

import androidx.compose.ui.graphics.Color

val LightTeal = Color(0xFF03DAC5)
val Teal = Color(0xFF009485)
val DarkTeal = Color(0xFF007A6C)
val Amber = Color(0xFFFFAA00)
val DarkBG = Color(0xFF232931)
