package fr.enssat.tregorrando.gourvesguennoc.data

data class Hike(
    val type: String,
    val properties: HikeProperties,
    val geometry: HikeGeometry?
) {
    override fun toString(): String {
        return """
        {
            "type": "Feature",
            "properties": $properties,
            "geometry": $geometry
        }
        """.trimIndent()
    }
}
