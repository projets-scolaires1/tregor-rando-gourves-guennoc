package fr.enssat.tregorrando.gourvesguennoc.ui.compose

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.guru.fontawesomecomposelib.FaIcon
import com.guru.fontawesomecomposelib.FaIconType

@Composable
fun TypeFaIcon(faIcon: FaIconType, modifier: Modifier = Modifier) {
    FaIcon(
        faIcon = faIcon,
        modifier = modifier,
        size = 40.dp,
        tint = Color.White
    )
}
