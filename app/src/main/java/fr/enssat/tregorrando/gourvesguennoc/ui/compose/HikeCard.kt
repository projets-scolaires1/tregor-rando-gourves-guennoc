package fr.enssat.tregorrando.gourvesguennoc.ui.compose

import android.content.Intent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.guru.fontawesomecomposelib.FaIcons
import fr.enssat.tregorrando.gourvesguennoc.activities.MapActivity
import fr.enssat.tregorrando.gourvesguennoc.data.HikeProperties
import fr.enssat.tregorrando.gourvesguennoc.data.HikeType
import fr.enssat.tregorrando.gourvesguennoc.ui.theme.Amber

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HikeCard(hike: HikeProperties) {
    val context = LocalContext.current

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        elevation = 10.dp,
        onClick = {
            val intent = Intent(context, MapActivity::class.java)
            intent.putExtra("hike_id", hike.id)
            ContextCompat.startActivity(context, intent, null)
        }
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
        ) {
            Row(
                modifier = Modifier.padding(vertical = 16.dp, horizontal = 0.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                when (hike.type) {
                    HikeType.PEDESTRIAN -> {
                        TypeFaIcon(FaIcons.Hiking)
                    }
                    HikeType.HORSE -> {
                        TypeFaIcon(FaIcons.Horse)
                    }
                    HikeType.BIKE -> {
                        TypeFaIcon(FaIcons.Biking)
                    }
                    HikeType.MOUNTAIN_BIKE -> {
                        TypeFaIcon(FaIcons.Biking)
                    }
                    HikeType.ALL_HIKE_TYPE -> {
                        TypeFaIcon(FaIcons.Atlas)
                    }
                }

                Text(
                    hike.name,
                    fontWeight = FontWeight.Bold,
                    color = Amber,
                    modifier = Modifier.padding(
                        horizontal = 24.dp,
                        vertical = 0.dp
                    )
                )
            }
            Text(hike.address, fontStyle = FontStyle.Italic)
        }
    }
}
