package fr.enssat.tregorrando.gourvesguennoc.viewmodels

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.enssat.tregorrando.gourvesguennoc.data.Hike
import fr.enssat.tregorrando.gourvesguennoc.network.DatarmorApiStatus
import fr.enssat.tregorrando.gourvesguennoc.network.DatarmorNetworkService
import kotlinx.coroutines.launch

class MapViewModel(val intent: Intent) : ViewModel() {
    private var id: Int? = null

    private val _status = MutableLiveData<DatarmorApiStatus>()
    val status: LiveData<DatarmorApiStatus>
        get() = _status

    private val _properties = MutableLiveData<Hike>()
    val properties: LiveData<Hike>
        get() = _properties

    init {
        viewModelScope.launch {
            _status.value = DatarmorApiStatus.LOADING

            try {
                id = intent.extras?.getInt("hike_id")
                _properties.value = id?.let { DatarmorNetworkService.getHikeDetails(it) }
                _status.value = DatarmorApiStatus.DONE
            } catch (e: Exception) {
                _status.value = DatarmorApiStatus.ERROR
                _properties.value = null
            }
        }
    }
}
