package fr.enssat.tregorrando.gourvesguennoc.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import fr.enssat.tregorrando.gourvesguennoc.R
import fr.enssat.tregorrando.gourvesguennoc.data.HikeType
import fr.enssat.tregorrando.gourvesguennoc.network.DatarmorApiStatus
import fr.enssat.tregorrando.gourvesguennoc.ui.compose.HikeCard
import fr.enssat.tregorrando.gourvesguennoc.ui.compose.MenuHeader
import fr.enssat.tregorrando.gourvesguennoc.ui.theme.TregorRandoTheme
import fr.enssat.tregorrando.gourvesguennoc.viewmodels.MenuViewModel

class MenuActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val menuViewModel: MenuViewModel by viewModels()

        setContent {
            TregorRandoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val status by menuViewModel.status.observeAsState(DatarmorApiStatus.ERROR)
                    val hikes by menuViewModel.properties.observeAsState()
                    val filterByType by menuViewModel.filterByType.observeAsState()
                    val filterByName by menuViewModel.filterByName.observeAsState()

                    val searchQuery = filterByName ?: ""

                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .paint(
                                painter = painterResource(id = R.drawable.bg_home),
                                contentScale = ContentScale.Crop,
                                colorFilter = ColorFilter.colorMatrix(ColorMatrix().apply {
                                    setToSaturation(
                                        0f
                                    )
                                })
                            )
                    ) {
                        MenuHeader(menuViewModel)

                        when (status) {
                            DatarmorApiStatus.LOADING -> {
                                Column(
                                    modifier = Modifier.fillMaxSize(),
                                    verticalArrangement = Arrangement.Center,
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    CircularProgressIndicator(color = Color.White)
                                }
                            }
                            DatarmorApiStatus.DONE -> {
                                LazyColumn {
                                    hikes?.let {
                                        items(it.filter { hike ->
                                            hike.properties.name.contains(
                                                searchQuery,
                                                ignoreCase = true
                                            )
                                        }) { hike ->
                                            if (filterByType == HikeType.ALL_HIKE_TYPE || hike.properties.type == filterByType) {
                                                HikeCard(hike.properties)
                                            }
                                        }
                                    }
                                }
                            }
                            DatarmorApiStatus.ERROR -> {
                                Column(
                                    modifier = Modifier.fillMaxSize(),
                                    verticalArrangement = Arrangement.Center,
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Text(
                                        text = "Une erreur est survenue. Etes-vous connecté à Internet ?",
                                        style = MaterialTheme.typography.body2,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = 16.dp, vertical = 24.dp)
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
