package fr.enssat.tregorrando.gourvesguennoc.map

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import fr.enssat.tregorrando.gourvesguennoc.BuildConfig
import fr.enssat.tregorrando.gourvesguennoc.R
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.views.MapView

object MapLifecycle {

    /**
     * Return MapView configuration
     */
    @Composable
    fun rememberMapViewWithLifecycle(
        hike: String
    ): MapView {
        val context = LocalContext.current

        val mapView = remember {
            MapView(context).apply {
                id = R.id.map
                clipToOutline = true
                setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE) // Define Tile source
                setMultiTouchControls(true)

                LoadHikeOnMapService().loadHikePathById(this, hike)
            }
        }

        // Makes MapView follow the lifecycle of this composable
        val lifecycleObserver = rememberMapLifecycleObserver(mapView)
        val lifecycle = LocalLifecycleOwner.current.lifecycle
        DisposableEffect(lifecycle) {
            lifecycle.addObserver(lifecycleObserver)
            onDispose {
                lifecycle.removeObserver(lifecycleObserver)
            }
        }

        return mapView
    }

    @Composable
    private fun rememberMapLifecycleObserver(mapView: MapView): LifecycleEventObserver =
        remember(mapView) {
            LifecycleEventObserver { _, event ->
                when (event) {
                    Lifecycle.Event.ON_RESUME -> {
                        // Define necessary UserAgent to display the map
                        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
                        mapView.onResume()
                    }
                    Lifecycle.Event.ON_PAUSE -> mapView.onPause()
                    else -> {}
                }
            }
        }
}
