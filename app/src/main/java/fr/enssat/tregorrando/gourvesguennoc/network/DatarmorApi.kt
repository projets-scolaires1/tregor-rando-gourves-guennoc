package fr.enssat.tregorrando.gourvesguennoc.network

import fr.enssat.tregorrando.gourvesguennoc.data.HikeCollection
import retrofit2.http.GET
import retrofit2.http.Query

interface DatarmorApi {
    companion object {
        const val BASE_URL = "https://datarmor.cotesdarmor.fr:443/dataserver/cg22/data/"
        const val HIKES_NB = "50"
        const val PROPERTIES = "iti_nom,iti_com_li,iti_id,iti_vocati"
    }

    @GET("itineraires_randonnees_ltc?&\$format=geojson")
    suspend fun getHikeCollection(
        @Query("\$select") select: String = "$PROPERTIES,geometry",
        @Query("\$top") max: String = HIKES_NB
    ): HikeCollection

    @GET("itineraires_randonnees_ltc?&\$format=geojson")
    suspend fun getHikeCollectionByFilter(
        @Query("\$select") select: String = "$PROPERTIES,geometry",
        @Query("\$top") max: String = HIKES_NB,
        @Query("\$filter") filter: String
    ): HikeCollection
}
