package fr.enssat.tregorrando.gourvesguennoc.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import fr.enssat.tregorrando.gourvesguennoc.R
import fr.enssat.tregorrando.gourvesguennoc.ui.theme.Amber
import fr.enssat.tregorrando.gourvesguennoc.ui.theme.TregorRandoTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            TregorRandoTheme {
                //val image = painterResource(id = R.drawable.bg_home)
                //val imageModifier = Modifier.drawBackground(image)
                // A surface container using the 'background' color from the theme
                Surface(
                    color = MaterialTheme.colors.background
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .paint(
                                painter = painterResource(id = R.drawable.bg_home),
                                contentScale = ContentScale.Crop,
                                colorFilter = ColorFilter.colorMatrix(ColorMatrix().apply {
                                    setToSaturation(
                                        0f
                                    )
                                })
                            )
                    ) {
                        Text(
                            text = "Tregor Rando",
                            style = MaterialTheme.typography.h1,
                            color = Amber,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 4.dp, vertical = 80.dp)
                        )

                        Column(
                            modifier = Modifier
                                .fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Button(
                                modifier = Modifier.padding(16.dp),
                                onClick = {
                                    val intent = Intent(this@MainActivity, MenuActivity::class.java)
                                    startActivity(intent)
                                },
                                contentPadding = PaddingValues(16.dp)
                            ) {
                                Text("Sélectionner une randonnée")
                            }
                        }
                    }
                }
            }
        }
    }

}
