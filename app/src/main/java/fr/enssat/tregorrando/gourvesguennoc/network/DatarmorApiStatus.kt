package fr.enssat.tregorrando.gourvesguennoc.network

enum class DatarmorApiStatus {
    LOADING, ERROR, DONE
}
