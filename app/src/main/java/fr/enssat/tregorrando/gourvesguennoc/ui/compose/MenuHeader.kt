package fr.enssat.tregorrando.gourvesguennoc.ui.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import fr.enssat.tregorrando.gourvesguennoc.data.HikeType
import fr.enssat.tregorrando.gourvesguennoc.ui.theme.DarkBG
import fr.enssat.tregorrando.gourvesguennoc.viewmodels.MenuViewModel

@Composable
fun MenuHeader(menuViewModel: MenuViewModel) {
    Column(
        modifier = Modifier
            .background(DarkBG)
            .fillMaxWidth()
            .padding(0.dp)
    ) {
        Text(
            text = "Choisir son parcours",
            style = MaterialTheme.typography.h1,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 4.dp, vertical = 16.dp)
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 4.dp, vertical = 16.dp)
        ) {
            FilterByName(menuViewModel)
            FilterByType(menuViewModel)
        }
    }
}

@Composable
fun FilterByName(menuViewModel: MenuViewModel) {
    val filterByName by menuViewModel.filterByName.observeAsState()
    val searchQuery = filterByName ?: ""

    TextField(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 64.dp),
        value = searchQuery,
        onValueChange = { menuViewModel.filterByName.value = it }
    )
}

@Composable
fun FilterByType(menuViewModel: MenuViewModel) {
    val filterByType by menuViewModel.filterByType.observeAsState()

    var expanded by remember { mutableStateOf(false) }
    val items = HikeType.values()

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 64.dp)
    ) {
        Button(
            onClick = { expanded = true },
            modifier = Modifier
                .width(200.dp)
                .align(Alignment.Center)
        ) {
            filterByType?.let { Text(it.name) }
        }

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .width(200.dp)
                .align(Alignment.Center)
        ) {
            items.forEachIndexed { index, type ->
                DropdownMenuItem(
                    onClick = {
                        menuViewModel.filterByType.value = HikeType.values()[index]
                        expanded = false
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(0.dp)
                ) {
                    Text(text = type.name, textAlign = TextAlign.Center)
                }
            }
        }
    }
}
