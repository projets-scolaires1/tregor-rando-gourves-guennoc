package fr.enssat.tregorrando.gourvesguennoc.data

import com.squareup.moshi.Json

data class HikeProperties(
    @Json(name = "iti_nom") val name: String,
    @Json(name = "iti_com_li") val address: String,
    @Json(name = "iti_id") val id: Int,
    @Json(name = "iti_vocati") val type: HikeType
) {
    override fun toString(): String {
        return """
        {
            "iti_com_li": "$address",
            "iti_id": $id,
            "iti_vocati": "$type",
            "iti_nom": "$name"
        }
        """.trimIndent()
    }
}
