package fr.enssat.tregorrando.gourvesguennoc.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fr.enssat.tregorrando.gourvesguennoc.data.Hike
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.Executors

object DatarmorNetworkService {

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(DatarmorApi.BASE_URL)
        .client(DatarmorNetworkConstructor.getHttpClient())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .callbackExecutor(Executors.newSingleThreadExecutor())
        .build()
        .create(DatarmorApi::class.java)

    /*******************
     * API CALLS
     *******************/

    /**
     * Get all Hikes
     *
     * @return List<HikeDTO>
     */
    suspend fun getAllHikes(): List<Hike> = api.getHikeCollection().features

    /**
     * Get Hike details by its id
     *
     * @param id Int
     *
     * @return HikeDTO
     */
    suspend fun getHikeDetails(id: Int): Hike = api.getHikeCollectionByFilter(
        filter = "iti_id eq '${id}'",
        max = "1"
    ).features.first()
}
