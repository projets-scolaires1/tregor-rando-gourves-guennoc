# TREGOR RANDO

> Groupe :
> - [x] Pierre-Yves Guennoc
> - [x] Steven Gourves
>

[[_TOC_]]

## Description

Tregor Rando est une application android permettant de gérer les randonnées du Trégor. Elle permet de consulter la liste des randonnées, faire une recherche sur le nom des parcours et par type de parcours (Pédestre, VTT, Vélo, Equestre).

## Utilisation

Pour utiliser l'application, il faut télécharger le fichier APK sur votre téléphone et l'installer (disponible dans le dossier `apk`).

Pour générer le fichier APK depuis Android Studio, il faut aller dans le menu `Build > Build Bundle(s)/APK(s) > Build APK(s)` et suivre les instructions.

## Interface utilisateur

Lors du lancement de l'application, l'utilisateur est redirigé vers la page d'accueil (cf. Figure 1).

![Figure 1 : Page d'accueil](docs/accueil.jpg "Figure 1 : Page d'accueil")

Il peut alors choisir de consulter la liste des randonnées (cf. Figure 2). Sur la page de la liste des randonnées, l'utilisateur peut choisir de filtrer les randonnées par type de parcours et faire une recherche sur le nom des parcours.

![Figure 2 : Liste des randonnées](docs/page_randonnees.jpg)

Une fois choisi, il peut consulter le tracé de la randonnée sur une carte (cf. Figure 3).

![Figure 3 : Carte de la randonnée](docs/parcours.jpg)

## Développement

### Structure du projet

L'arborescence du projet est celui d'un projet Android Studio, tel que (seuls les dossiers et fichiers pertinents sont affichés) :

```txt
.
├── app
│   ├── build.gradle
│   ├── src
│   │   ├── main                                            # Code source de l'application
│   │   │   ├── AndroidManifest.xml
│   │   │   ├── java.fr.enssat.tregorrando.gourvesguennoc
│   │   │   │   ├── activities                              # Activités de l'application (les pages évoquées précédemment)
│   │   │   │   ├── data                                    # Différents types de données (les `data class`)
│   │   │   │   ├── map                                     # Gestion de la carte
│   │   │   │   ├── network                                 # Appel à l'API Datarmor (moshi et retrofit)
│   │   │   │   ├── ui                                      # Gestion de l'interface utilisateur
│   │   │   │   │   ├── compose                             # Composants de l'interface utilisateur (les `@Composable`)
│   │   │   │   │   └── theme                               # Thème de l'application
│   │   │   │   └── viewmodels                              # Gestion des données de l'application
│   │   │   └── res
│   │   │       ├── drawable                                # Images utilisées dans l'application
│   │   │       │   ├── bg_home.jpg
│   │   │       │   └── ...
│   │   │       ├── layout                                  # Fichiers XML des pages de l'application
│   │   │       ├── values                                  # Fichiers XML de configuration du thème
│   │   │       └── ...
│   │   └── ...
│   └── ...
├── apk                                                     # Dossier contenant le fichier APK de l'application
├── docs                                                    # Fichiers de documentation (ex. : images du README)
└── README.md                                               # Documentation du projet
```

### Choix techniques

Il y a plusieurs raisons pour lesquelles ces choix technologiques ont été faits pour le développement de l'application Android.

* **OpenStreetMap** (osmdroid) a été utilisé comme source de carte, car c'est une solution libre et open-source qui permet de facilement intégrer une carte interactive dans une application Android ;

* **Moshi** et **Retrofit** ont été utilisés pour réaliser l'intégration avec une API JSON. Moshi est une bibliothèque de conversion de données JSON pour Android qui offre une syntaxe concise et facile à utiliser pour convertir les données JSON en objets Java et inversement. Retrofit est une bibliothèque de client HTTP qui facilite la création de requêtes HTTP et la gestion des réponses ;

* **KmlDocument** a été utilisé pour simplifier l'implémentation d'une randonnée sur la carte, car il permet de facilement charger et afficher des fichiers KML (Keyhole Markup Language) depuis une string GeoJSON qui décrivent des chemins et des points d'intérêt sur la carte ;

* **Jetpack Compose** a été utilisé pour l'interface utilisateur (UI) et l'expérience utilisateur (UX) car c'est une technologie recommandée par Google et qui permet de faciliter le développement d'interfaces utilisateur pour les applications Android. Elle se base sur une approche par composant, ce qui peut simplifier le développement et améliorer la lisibilité du code (cf. dossier `app/src/main/java/fr/enssat/tregorrando/gourvesguennoc/ui/compose`).

### Problèmes rencontrés

1. Chargements de toutes les randonnées trop longs

    Lors du chargement de toutes les randonnées, l'application charge toutes les données de l'API Datarmor. Cela prend beaucoup de temps et cela génère des erreurs de timeout. Pour résoudre ce problème, nous avons décidé de ne charger que les 50 premières randonnées mais ce n'est pas une solution optimale. Les pistes d'amélioration ont été de récupérer les données par page (avec un nombre de randonnées par page plus petit) où les données sont chargées au fur et à mesure de la navigation dans la liste des randonnées.

    Malheureusement, nous n'avons pas eu le temps de mettre en place ces solutions, car nous manquons de temps et de connaissances sur l'API Datarmor et sur Android.

2. Récupération du tracé GeoJSON et implémentation du parcours sur la carte

    Une fois les données de la randonnée chargées, il fallait le convertir en `string` pour l'utiliser avec la librairie `KmlDocument` qui permet de convertir un fichier `string` en `FolderOverlay` utilisable avec la librairie `osmdroid`. Cependant, nous n'arrivons pas à récupérer l'ensemble des données GeoJSON dans une seule variable et garder les données `properties` utiliser pour afficher des informations (le type de données `Hike`). Nous avons donc décidé de récupérer toutes les données avec des types (`Hike`, `HikeProperties`, `HikeGeometry`) et de surcharger la méthode `toString()` de la classe `Hike` pour récupérer le `string` au format GeoJSON.

3. Utilisation de Moshi/Retrofit avec l'API Datarmor

    Nous avons rencontré des problèmes avec l'utilisation de Moshi/Retrofit avec l'API Datarmor. En effet, il y avait des problèmes concernants la validité des certificats SSL. Nous avons donc utilisé la librairie `okhttp3` pour configurer l'objet `X509TrustManager`.

4. Utilisation de la librairie `osmdroid`

    Il y a eu un blocage concernant l'affichage de la carte, car il fallait définir le `UserAgent` comme suivant :

    ```java
    // Define necessary UserAgent to display the map
    Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
    ```

5. Utilisation de l'API Datarmor

    Comme il n'y avait (presque) aucune documentation sur l'API Datarmor ([le lien officiel](https://datarmor.cotesdarmor.fr/documentation-/-api) contenait très peu d'éléments), nous avons dû faire des tests et de nombreuses recherches afin de deviner comment l'utiliser. Au final, nous avons compris que c'était une API de type `OData` (en se basant sur [la documentation suivante](https://help.salesforce.com/s/articleView?language=en_US&id=sf.odata_query_string_options.htm&type=5#system_query_option_filter)).
